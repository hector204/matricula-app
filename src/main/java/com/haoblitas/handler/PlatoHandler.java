package com.haoblitas.handler;

import static org.springframework.web.reactive.function.BodyInserters.fromValue;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.haoblitas.document.Plato;
import com.haoblitas.dto.ValidacionDTO;
import com.haoblitas.service.IPlatoService;
import com.haoblitas.validators.RequestValidator;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class PlatoHandler {

	@Autowired
	private IPlatoService service;
	
	@Autowired
	private Validator validador;
	
	@Autowired
	private RequestValidator validadorGeneral;
	
	public Mono<ServerResponse> listar(ServerRequest request){
		return ServerResponse
				.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(service.listar(),Plato.class);
	}
	
	public Mono<ServerResponse> listarPorId(ServerRequest request){
		String id = request.pathVariable("id");
		return service.listarPorId(id)
				.flatMap(p-> ServerResponse
						.ok()
						.contentType(MediaType.APPLICATION_STREAM_JSON)
						.body(fromValue(p))
				)
				.switchIfEmpty(ServerResponse.notFound().build());
	}
	
	public Mono<ServerResponse> registrar(ServerRequest req){
		Mono<Plato> platoMono = req.bodyToMono(Plato.class);

		//CON VALIDACION MANUAL
		/*return platoMono.flatMap(p->{
					Errors errores = new BeanPropertyBindingResult(p, Plato.class.getName());
					validador.validate(p, errores);
					if(errores.hasErrors()) {
						return Flux.fromIterable(errores.getFieldErrors())
								.map(error-> new ValidacionDTO(error.getField(),error.getDefaultMessage()))
								.collectList()
								.flatMap(listaErrores->{
									return ServerResponse.badRequest()
											.contentType(MediaType.APPLICATION_STREAM_JSON)
											.body(fromValue(listaErrores));
								});
								
					}else {
						return service.registrar(p)
								.flatMap(x -> ServerResponse
										.created(URI.create(req.uri().toString().concat("/").concat(x.getId())))
										.contentType(MediaType.APPLICATION_STREAM_JSON)
										.body(fromValue(x))
								);		
					}
				});*/
		
		//CON VALIDACION AUTOMATICA
		return platoMono
				.flatMap(this.validadorGeneral::validar)				
				.flatMap(p->service.registrar(p))
				.flatMap(p -> ServerResponse
						.created(URI.create(req.uri().toString().concat("/").concat(p.getId())))
						.contentType(MediaType.APPLICATION_STREAM_JSON)
						.body(fromValue(p))
				);
		
		//SIN VALIDACION
		/*return platoMono.flatMap(p -> {
					return service.registrar(p);
				})
				.flatMap(p -> ServerResponse
							.created(URI.create(req.uri().toString().concat("/").concat(p.getId())))
							.contentType(MediaType.APPLICATION_STREAM_JSON)
							.body(fromValue(p))
				);*/
	}
	
	public Mono<ServerResponse> modificar(ServerRequest req){		
		Mono<Plato> platoMono = req.bodyToMono(Plato.class);
		return platoMono
				.flatMap(this.validadorGeneral::validar)	
				.flatMap(p -> service.modificar(p))
				.flatMap(p -> ServerResponse
							.ok()
							.contentType(MediaType.APPLICATION_STREAM_JSON)
							.body(fromValue(p))
				).switchIfEmpty(ServerResponse.notFound().build());					
	}
	
	public Mono<ServerResponse> eliminar(ServerRequest req){
		String id = req.pathVariable("id");
		return service.listarPorId(id)
				.flatMap(p -> service.eliminar(p.getId())
							.then(ServerResponse.noContent().build())						
				)
				.switchIfEmpty(ServerResponse.notFound().build());		
	}
}
