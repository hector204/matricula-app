package com.haoblitas.handler;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.haoblitas.document.Cliente;
import com.haoblitas.service.IClienteService;
import com.haoblitas.validators.RequestValidator;

import reactor.core.publisher.Mono;

import static org.springframework.web.reactive.function.BodyInserters.fromValue;

@Component
public class ClienteHandler {

	@Autowired
	private IClienteService service;
	
	@Autowired
	private RequestValidator validadorGeneral;
	
	public Mono<ServerResponse> listar(ServerRequest request){
		return ServerResponse
				.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(service.listar(),Cliente.class);
	}
	
	public Mono<ServerResponse> listarPorId(ServerRequest request){
		String id = request.pathVariable("id");
		return service.listarPorId(id)
				.flatMap(p-> ServerResponse
						.ok()
						.contentType(MediaType.APPLICATION_STREAM_JSON)
						.body(fromValue(p))
				)
				.switchIfEmpty(ServerResponse.notFound().build());
	}
	
	public Mono<ServerResponse> registrar(ServerRequest req){
		Mono<Cliente> clienteMono = req.bodyToMono(Cliente.class);
		return clienteMono
				.flatMap(this.validadorGeneral::validar)	
				.flatMap(p -> service.registrar(p))
				.flatMap(p -> ServerResponse
							.created(URI.create(req.uri().toString().concat("/").concat(p.getId())))
							.contentType(MediaType.APPLICATION_STREAM_JSON)
							.body(fromValue(p))
				);
	}
	
	public Mono<ServerResponse> modificar(ServerRequest req){		
		Mono<Cliente> clienteMono = req.bodyToMono(Cliente.class);
		return clienteMono
				.flatMap(this.validadorGeneral::validar)	
				.flatMap(p ->service.modificar(p))
				.flatMap(p -> ServerResponse
							.ok()
							.contentType(MediaType.APPLICATION_STREAM_JSON)
							.body(fromValue(p))
				).switchIfEmpty(ServerResponse.notFound().build());					
	}
	
	public Mono<ServerResponse> eliminar(ServerRequest req){
		String id = req.pathVariable("id");
		return service.listarPorId(id)
				.flatMap(p -> service.eliminar(p.getId())
							.then(ServerResponse.noContent().build())						
				)
				.switchIfEmpty(ServerResponse.notFound().build());		
	}
}
