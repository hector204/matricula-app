package com.haoblitas.filters;

import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;

import reactor.core.publisher.Mono;

@Component
public class EjemploFilter implements WebFilter{

	@Override
	public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
		
		//logica prog.
		
		exchange.getResponse().getHeaders().add("filtro1", "contenido-filtro1");
		
		return chain.filter(exchange);
	}

}
