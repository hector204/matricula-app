package com.haoblitas.service;

import org.springframework.data.domain.Pageable;

import com.haoblitas.document.Factura;
import com.haoblitas.pagination.PageSupport;

import reactor.core.publisher.Mono;

public interface IFacturaService extends ICRUD<Factura, String> {

	Mono<PageSupport<Factura>> listarPagina(Pageable page);

}
