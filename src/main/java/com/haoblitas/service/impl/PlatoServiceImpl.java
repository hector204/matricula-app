package com.haoblitas.service.impl;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.haoblitas.document.Plato;
import com.haoblitas.pagination.PageSupport;
import com.haoblitas.repo.IPlatoRepo;
import com.haoblitas.service.IPlatoService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class PlatoServiceImpl implements IPlatoService {

	@Autowired
	private IPlatoRepo platoRepo;

	@Override
	public Mono<Plato> registrar(Plato t) {
		return platoRepo.save(t);
	}

	@Override
	public Mono<Plato> modificar(Plato t) {
		return platoRepo.save(t);
	}

	@Override
	public Flux<Plato> listar() {
		return platoRepo.findAll();
	}

	@Override
	public Mono<Plato> listarPorId(String v) {
		return platoRepo.findById(v);
	}

	@Override
	public Mono<Void> eliminar(String v) {
		return platoRepo.deleteById(v);
	}

	@Override
	public Flux<Plato> buscarPorNombre(String nombre) {
		//SELECT * FROM PLATO p WHERE p.nombre = ?
		return platoRepo.findByNombre(nombre);
	}

	@Override
	public Mono<PageSupport<Plato>> listarPage(Pageable page) {
		return platoRepo.findAll()
		        .collectList()
		        .map(list -> new PageSupport<>(
		        	list
		                .stream()
		                .skip(page.getPageNumber() * page.getPageSize())
		                .limit(page.getPageSize())
		                .collect(Collectors.toList()),
		            page.getPageNumber(), page.getPageSize(), list.size()
		            )
		        );
	}

}
