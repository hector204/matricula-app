package com.haoblitas.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.haoblitas.document.Matricula;
import com.haoblitas.repo.IMatriculaRepo;
import com.haoblitas.service.IMatriculaService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class MatriculaServiceImpl implements IMatriculaService {

	@Autowired
	private IMatriculaRepo repo;

	@Override
	public Mono<Matricula> registrar(Matricula t) {
		return repo.save(t);
	}

	@Override
	public Mono<Matricula> modificar(Matricula t) {
		return repo.save(t);
	}

	@Override
	public Flux<Matricula> listar() {
		return repo.findAll();
	}

	@Override
	public Mono<Matricula> listarPorId(String v) {
		return repo.findById(v);
	}

	@Override
	public Mono<Void> eliminar(String v) {
		return repo.deleteById(v);
	}

}
