package com.haoblitas.service.impl;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.haoblitas.document.Factura;
import com.haoblitas.pagination.PageSupport;
import com.haoblitas.repo.IFacturaRepo;
import com.haoblitas.service.IFacturaService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class FacturaServiceImpl implements IFacturaService {

	@Autowired
	private IFacturaRepo repo;

	@Override
	public Mono<Factura> registrar(Factura t) {
		return repo.save(t);
	}

	@Override
	public Mono<Factura> modificar(Factura t) {
		return repo.save(t);
	}

	@Override
	public Flux<Factura> listar() {
		return repo.findAll();
	}

	public Mono<Factura> listarPorId(String v) {
		return repo.findById(v);
	}

	@Override
	public Mono<Void> eliminar(String v) {
		return repo.deleteById(v);
	}

	@Override
	public Mono<PageSupport<Factura>> listarPagina(Pageable page) {
		
		return repo.findAll()
				.collectList()
				.map(lista -> new PageSupport<>(
						lista
						.stream()
						.skip(page.getPageNumber()*page.getPageSize())
						.limit(page.getPageSize())
						.collect(Collectors.toList()),
						page.getPageNumber(),page.getPageSize(),lista.size()
						));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	

}
