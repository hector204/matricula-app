package com.haoblitas.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.haoblitas.document.Curso;
import com.haoblitas.repo.ICursoRepo;
import com.haoblitas.service.ICursoService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CursoServiceImpl implements ICursoService {

	@Autowired
	private ICursoRepo repo;
	
	@Override
	public Mono<Curso> registrar(Curso t) {
		return repo.save(t);
	}

	@Override
	public Mono<Curso> modificar(Curso t) {
		return repo.save(t);
	}

	@Override
	public Flux<Curso> listar() {
		return repo.findAll();
	}

	@Override
	public Mono<Curso> listarPorId(String v) {
		return repo.findById(v);
	}

	@Override
	public Mono<Void> eliminar(String v) {
		return repo.deleteById(v);
	}

}
