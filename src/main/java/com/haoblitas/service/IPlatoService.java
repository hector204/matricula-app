package com.haoblitas.service;

import org.springframework.data.domain.Pageable;

import com.haoblitas.document.Plato;
import com.haoblitas.pagination.PageSupport;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface IPlatoService extends ICRUD<Plato, String> {

	Flux<Plato> buscarPorNombre(String nombre);

	Mono<PageSupport<Plato>> listarPage(Pageable page);

}
