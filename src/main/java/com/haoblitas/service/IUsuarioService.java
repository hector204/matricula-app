package com.haoblitas.service;

import com.haoblitas.document.Usuario;
import com.haoblitas.security.User;

import reactor.core.publisher.Mono;

public interface IUsuarioService extends ICRUD<Usuario, String> {

	Mono<User> buscarPorUsuario(String usuario);

}