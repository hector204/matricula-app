package com.haoblitas.controller;

import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.linkTo;
import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.methodOn;
import static reactor.function.TupleUtils.function;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Links;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.haoblitas.document.Factura;
import com.haoblitas.service.IFacturaService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/facturas")
public class FacturaController {
	
	@Autowired
	private IFacturaService service;
	
	@GetMapping
	public Mono<ResponseEntity<Flux<Factura>>>  listar() {
		
		Flux<Factura> facturas = service.listar();
		
		return Mono.just(
				 ResponseEntity
				.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(facturas)
		);
	}

	@GetMapping("/{id}")
	public Mono<ResponseEntity<Factura>> listarPorId(@PathVariable("id") String id) {
		
		return service.listarPorId(id)
				//defaultIfEmpty(new Factura())
				.map(p -> ResponseEntity
						.ok()
						.contentType(MediaType.APPLICATION_STREAM_JSON)
						.body(p))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}

	@PostMapping
	public Mono<ResponseEntity<Factura>> registrar(@Valid @RequestBody Factura plato, final ServerHttpRequest request) {
		
		return service.registrar(plato)
				.map(p->ResponseEntity
						.created(URI.create(request.getURI().toString().concat("/").concat(p.getId())))
						.contentType(MediaType.APPLICATION_STREAM_JSON)
						.body(p));
	}

	@PutMapping
	public Mono<ResponseEntity<Factura>> modificar(@Valid @RequestBody Factura plato) {
		
		return service.modificar(plato)
				.map(p->ResponseEntity
						.ok()
						.contentType(MediaType.APPLICATION_STREAM_JSON)
						.body(p))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}

	@DeleteMapping("/{id}")
	public Mono<ResponseEntity<Void>> eliminar(@PathVariable("id") String id) {
		
		return service.listarPorId(id)
				.flatMap(p->{
					return service.eliminar(p.getId())
							.then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
				})
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	//https://github.com/spring-projects/spring-hateoas/blob/a89e57eed7e12819b87be7f1b977dddfd432541d/src/test/java/org/springframework/hateoas/support/WebFluxEmployeeController.java#L101-L106
	//private Factura platoHateoas;
	@GetMapping("/hateoas/{id}")
	public Mono<EntityModel<Factura>> listarHateoasPorId(@PathVariable("id") String id) {
		
		Mono<Link> selfLink = linkTo(methodOn(FacturaController.class).listarHateoasPorId(id)).withSelfRel().toMono();	
		Mono<Link> selfLink2 = linkTo(methodOn(FacturaController.class).listarHateoasPorId(id)).withSelfRel().toMono();
		
		//PRACTICA FACIL
		/*return service.listarPorId(id)
				.flatMap(p -> {
					this.platoHateoas = p;
					return selfLink;})
				.map(links -> {
					return new EntityModel<>(this.platoHateoas, links);
				}
		);*/
		
		//PRACTICA INTERMEDIA
		/*return service.listarPorId(id)
				.flatMap(p -> {
					return selfLink.map(links -> new EntityModel<>(p, links));
				});*/
		
		//PRACTICA IDEAL | 1 link
		/*return service.listarPorId(id)
				.zipWith(selfLink, (p, links) -> {
					return new EntityModel<>(p, links);
				});*/
		
		//PRACTICA IDEAL | 2+ link
		return selfLink.zipWith(selfLink2)
						.map(function((izq, der) -> Links.of(izq, der)))
						.zipWith(service.listarPorId(id), (links, p) -> {
							return new EntityModel<>(p, links);
						});
		
	}

}
