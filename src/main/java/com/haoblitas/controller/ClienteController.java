package com.haoblitas.controller;

import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.linkTo;
import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.methodOn;
import static reactor.function.TupleUtils.function;

import java.io.File;
import java.net.URI;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Links;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;

import com.haoblitas.document.Cliente;
import com.haoblitas.service.IClienteService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/clientes")
public class ClienteController {
	
	@Autowired
	private IClienteService service;
	
	@Value("${ruta.subida}")
	private String RUTA_SUBIDA;
	
	@GetMapping
	public Mono<ResponseEntity<Flux<Cliente>>>  listar() {
		
		Flux<Cliente> clientes = service.listar();
		
		return Mono.just(
				 ResponseEntity
				.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(clientes)
		);
	}

	@GetMapping("/{id}")
	public Mono<ResponseEntity<Cliente>> listarPorId(@PathVariable("id") String id) {
		
		return service.listarPorId(id)
				//defaultIfEmpty(new Cliente())
				.map(p -> ResponseEntity
						.ok()
						.contentType(MediaType.APPLICATION_STREAM_JSON)
						.body(p))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}

	@PostMapping
	public Mono<ResponseEntity<Cliente>> registrar(@Valid @RequestBody Cliente plato, final ServerHttpRequest request) {
		
		return service.registrar(plato)
				.map(p->ResponseEntity
						.created(URI.create(request.getURI().toString().concat("/").concat(p.getId())))
						.contentType(MediaType.APPLICATION_STREAM_JSON)
						.body(p));
	}

	@PutMapping
	public Mono<ResponseEntity<Cliente>> modificar(@Valid @RequestBody Cliente plato) {
		
		return service.modificar(plato)
				.map(p->ResponseEntity
						.ok()
						.contentType(MediaType.APPLICATION_STREAM_JSON)
						.body(p))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}

	@DeleteMapping("/{id}")
	public Mono<ResponseEntity<Void>> eliminar(@PathVariable("id") String id) {
		
		return service.listarPorId(id)
				.flatMap(p->{
					return service.eliminar(p.getId())
							.then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
				})
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	//https://github.com/spring-projects/spring-hateoas/blob/a89e57eed7e12819b87be7f1b977dddfd432541d/src/test/java/org/springframework/hateoas/support/WebFluxEmployeeController.java#L101-L106
	//private Cliente platoHateoas;
	@GetMapping("/hateoas/{id}")
	public Mono<EntityModel<Cliente>> listarHateoasPorId(@PathVariable("id") String id) {
		
		Mono<Link> selfLink = linkTo(methodOn(ClienteController.class).listarHateoasPorId(id)).withSelfRel().toMono();	
		Mono<Link> selfLink2 = linkTo(methodOn(ClienteController.class).listarHateoasPorId(id)).withSelfRel().toMono();
		
		//PRACTICA FACIL
		/*return service.listarPorId(id)
				.flatMap(p -> {
					this.platoHateoas = p;
					return selfLink;})
				.map(links -> {
					return new EntityModel<>(this.platoHateoas, links);
				}
		);*/
		
		//PRACTICA INTERMEDIA
		/*return service.listarPorId(id)
				.flatMap(p -> {
					return selfLink.map(links -> new EntityModel<>(p, links));
				});*/
		
		//PRACTICA IDEAL | 1 link
		/*return service.listarPorId(id)
				.zipWith(selfLink, (p, links) -> {
					return new EntityModel<>(p, links);
				});*/
		
		//PRACTICA IDEAL | 2+ link
		return selfLink.zipWith(selfLink2)
						.map(function((izq, der) -> Links.of(izq, der)))
						.zipWith(service.listarPorId(id), (links, p) -> {
							return new EntityModel<>(p, links);
						});
		
	}
	
	//subida archivos
	@PostMapping("/subir/{id}")
	public Mono<ResponseEntity<Cliente>> subir(@PathVariable String id, @RequestPart FilePart file){
		return service.listarPorId(id)
				.flatMap(c -> {	
					c.setUrlFoto(UUID.randomUUID().toString() + "-" + file.filename());
					return file
						.transferTo(new File(RUTA_SUBIDA + c.getUrlFoto()))
						.then(service.registrar(c));
				})
				.map(c -> ResponseEntity
						.ok()
						.body(c)
				)
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}	

}
