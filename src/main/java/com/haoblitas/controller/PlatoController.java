package com.haoblitas.controller;

import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.linkTo;
import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.methodOn;
import static reactor.function.TupleUtils.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Links;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import com.haoblitas.document.Cliente;
import com.haoblitas.document.Plato;
import com.haoblitas.dto.PlatoClienteDTO;
import com.haoblitas.pagination.PageSupport;
import com.haoblitas.service.IClienteService;
import com.haoblitas.service.IPlatoService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@RestController
@RequestMapping("/platos")
public class PlatoController {

	private static final Logger log = LoggerFactory.getLogger(PlatoController.class);
	
	@Autowired
	private IPlatoService service;
	
	@Autowired
	private IClienteService clienteService;
	
	@PreAuthorize("@restAuthServiceImpl.tieneAcceso('listar')")
	@GetMapping
	public Mono<ResponseEntity<Flux<Plato>>>  listar() {
		
		service.listar().parallel().runOn(Schedulers.elastic())
						.subscribe(i -> log.info(i.toString()));
		
		service.listar().subscribe(i -> log.info(i.toString()));
		
		Flux<Plato> platos = service.listar();
		
		return Mono.just(
				 ResponseEntity
				.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(platos)
		);
	}

	@GetMapping("/{id}")
	public Mono<ResponseEntity<Plato>> listarPorId(@PathVariable("id") String id) {
		
		return service.listarPorId(id)
				//defaultIfEmpty(new Plato())
				.map(p -> ResponseEntity
						.ok()
						.contentType(MediaType.APPLICATION_STREAM_JSON)
						.body(p))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}

	@PostMapping
	public Mono<ResponseEntity<Plato>> registrar(@Valid @RequestBody Plato plato, final ServerHttpRequest request) {
		
		return service.registrar(plato)
				.map(p->ResponseEntity
						.created(URI.create(request.getURI().toString().concat("/").concat(p.getId())))
						.contentType(MediaType.APPLICATION_STREAM_JSON)
						.body(p));
	}

	@PutMapping
	public Mono<ResponseEntity<Plato>> modificar(@Valid @RequestBody Plato plato) {
		
		return service.modificar(plato)
				.map(p->ResponseEntity
						.ok()
						.contentType(MediaType.APPLICATION_STREAM_JSON)
						.body(p))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}

	@DeleteMapping("/{id}")
	public Mono<ResponseEntity<Void>> eliminar(@PathVariable("id") String id) {
		
		return service.listarPorId(id)
				.flatMap(p->{
					return service.eliminar(p.getId())
							.then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
				})
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	//https://github.com/spring-projects/spring-hateoas/blob/a89e57eed7e12819b87be7f1b977dddfd432541d/src/test/java/org/springframework/hateoas/support/WebFluxEmployeeController.java#L101-L106
	//private Plato platoHateoas;
	@GetMapping("/hateoas/{id}")
	public Mono<EntityModel<Plato>> listarHateoasPorId(@PathVariable("id") String id) {
		
		Mono<Link> selfLink = linkTo(methodOn(PlatoController.class).listarHateoasPorId(id)).withSelfRel().toMono();	
		Mono<Link> selfLink2 = linkTo(methodOn(PlatoController.class).listarHateoasPorId(id)).withSelfRel().toMono();
		
		//PRACTICA FACIL
		/*return service.listarPorId(id)
				.flatMap(p -> {
					this.platoHateoas = p;
					return selfLink;})
				.map(links -> {
					return new EntityModel<>(this.platoHateoas, links);
				}
		);*/
		
		//PRACTICA INTERMEDIA
		/*return service.listarPorId(id)
				.flatMap(p -> {
					return selfLink.map(links -> new EntityModel<>(p, links));
				});*/
		
		//PRACTICA IDEAL | 1 link
		/*return service.listarPorId(id)
				.zipWith(selfLink, (p, links) -> {
					return new EntityModel<>(p, links);
				});*/
		
		//PRACTICA IDEAL | 2+ link
		return selfLink.zipWith(selfLink2)
						.map(function((izq, der) -> Links.of(izq, der)))
						.zipWith(service.listarPorId(id), (links, p) -> {
							return new EntityModel<>(p, links);
						});
		
	}
	
	// PAGINADO
	@GetMapping("/pageable")
	public Mono<ResponseEntity<PageSupport<Plato>>> listarPageable(
			@RequestParam(name = "page", defaultValue="0") int page,
			@RequestParam(name = "size", defaultValue="10") int size
			){
		
		Pageable pageRequest = PageRequest.of(page, size);
		
		return service.listarPage(pageRequest)
				.map(p -> ResponseEntity
						.ok()
						.contentType(MediaType.APPLICATION_STREAM_JSON)
						.body(p)
				)
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	//Listado total de los platos
	//quiero extrar su id y genera un flujo distinto por cada ID
	@GetMapping("/client1")
	public Flux<Plato> listarClient1(){
		Flux<Plato> platosFlux = WebClient.create("http://localhost:8081/platos")
									.get()
									.retrieve()
									.bodyToFlux(Plato.class);
		
		return platosFlux
				.parallel()
				.runOn(Schedulers.elastic())
				.flatMap(p-> service.listarPorId(p.getId()))
				.ordered((p1,p2)-> (int)p1.getPrecio()-(int)p2.getPrecio());
	}
	
	@GetMapping("/client2")	
	public Mono<ResponseEntity<Flux<Plato>>> listarClient2(){
		Flux<Plato> fx = Flux.merge(service.listarPorId("5e684be52e221a2c95bf51a6"), service.listarPorId("5e684c46bd46c940e1749cec"))
				.parallel()
				.runOn(Schedulers.elastic())
				.ordered((p1, p2) -> (int)p1.getPrecio() - (int)p2.getPrecio());
		
		return Mono.just(ResponseEntity
							.ok()
							.contentType(MediaType.APPLICATION_STREAM_JSON)
							.body(fx)
		);
	}
	
	@GetMapping("/client3")
	public Mono<ResponseEntity<PlatoClienteDTO>> listarClient3() {
		Mono<Plato> platoMono = service.listarPorId("5e684b5b332a76147977a49d")
				.subscribeOn(Schedulers.elastic()).defaultIfEmpty(new Plato());
		
		Mono<Cliente> clienteMono = clienteService.listarPorId("5e5bb8cf9c044e40e76a2f48")
				.subscribeOn(Schedulers.elastic()).defaultIfEmpty(new Cliente());
		
		return Mono.zip(clienteMono, platoMono, PlatoClienteDTO::new)
				.map(pc -> ResponseEntity
						.ok()
						.contentType(MediaType.APPLICATION_STREAM_JSON)
						.body(pc)
				).defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
}
