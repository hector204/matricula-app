package com.haoblitas.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import com.haoblitas.document.Estudiante;
import com.haoblitas.service.IEstudianteService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@RestController
@RequestMapping("/estudiantes")
public class EstudianteController {

	@Autowired
	private IEstudianteService service;
	
	@GetMapping
	public Mono<ResponseEntity<Flux<Estudiante>>>  listar() {
		
		Flux<Estudiante> estudiantes = service.listar();
		
		return Mono.just(
				 ResponseEntity
				.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(estudiantes)
		);
	}
	
	@GetMapping("/{id}")
	public Mono<ResponseEntity<Estudiante>> listarPorId(@PathVariable("id") String id) {
		
		return service.listarPorId(id)
				.map(p -> ResponseEntity
						.ok()
						.contentType(MediaType.APPLICATION_STREAM_JSON)
						.body(p))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	@PostMapping
	public Mono<ResponseEntity<Estudiante>> registrar(@Valid @RequestBody Estudiante estudiante, final ServerHttpRequest request) {
		
		return service.registrar(estudiante)
				.map(p->ResponseEntity
						.created(URI.create(request.getURI().toString().concat("/").concat(p.getId())))
						.contentType(MediaType.APPLICATION_STREAM_JSON)
						.body(p));
	}
	
	@PutMapping
	public Mono<ResponseEntity<Estudiante>> modificar(@Valid @RequestBody Estudiante estudiante) {
		
		return service.modificar(estudiante)
				.map(p->ResponseEntity
						.ok()
						.contentType(MediaType.APPLICATION_STREAM_JSON)
						.body(p))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	@DeleteMapping("/{id}")
	public Mono<ResponseEntity<Void>> eliminar(@PathVariable("id") String id) {
		
		return service.listarPorId(id)
				.flatMap(p->{
					return service.eliminar(p.getId())
							.then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
				})
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	@GetMapping("/estudiante1")
	public Flux<Estudiante> estudiante1(){
//		Flux<Estudiante> estudiantesFlux = WebClient.create("http://localhost:8081/estudiantes")
//									.get()
//									.retrieve()
//									.bodyToFlux(Estudiante.class);
		
//		System.out.println(estudiantesFlux);
		
		return service.listar()
						.parallel()
						.runOn(Schedulers.elastic())
						.flatMap(p-> service.listarPorId(p.getId()))
						.ordered((p1,p2)-> p1.getEdad()-p2.getEdad());
		
//		return null;
		
//		return estudiantesFlux
//				.parallel()
//				.runOn(Schedulers.elastic())
//				.flatMap(p-> service.listarPorId(p.getId()))
//				.ordered((p1,p2)-> p1.getEdad()-p2.getEdad());
	}
	
	
	
}
