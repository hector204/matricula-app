package com.haoblitas.repo;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.haoblitas.document.Estudiante;

public interface IEstudianteRepo extends ReactiveMongoRepository<Estudiante, String>{

}
