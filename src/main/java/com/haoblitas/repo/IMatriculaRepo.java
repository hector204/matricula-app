package com.haoblitas.repo;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.haoblitas.document.Matricula;

public interface IMatriculaRepo extends ReactiveMongoRepository<Matricula, String> {

}
