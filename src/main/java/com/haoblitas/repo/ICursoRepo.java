package com.haoblitas.repo;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.haoblitas.document.Curso;

public interface ICursoRepo extends ReactiveMongoRepository<Curso, String>{

}
