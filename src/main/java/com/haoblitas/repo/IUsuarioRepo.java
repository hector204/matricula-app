package com.haoblitas.repo;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.haoblitas.document.Usuario;

import reactor.core.publisher.Mono;

public interface IUsuarioRepo extends ReactiveMongoRepository<Usuario, String>{

	 Mono<Usuario> findOneByUsuario(String usuario);

}

