package com.haoblitas.repo;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.haoblitas.document.Cliente;

public interface IClienteRepo extends ReactiveMongoRepository<Cliente, String> {

}
