package com.haoblitas.repo;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.haoblitas.document.Factura;

public interface IFacturaRepo extends ReactiveMongoRepository<Factura, String> {

}
