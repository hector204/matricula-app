package com.haoblitas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringReactorRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringReactorRestApplication.class, args);
	}

}
