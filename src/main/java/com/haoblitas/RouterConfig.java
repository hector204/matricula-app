package com.haoblitas;

import static org.springframework.web.reactive.function.server.RequestPredicates.DELETE;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RequestPredicates.PUT;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.haoblitas.handler.ClienteHandler;
import com.haoblitas.handler.CursoHandler;
import com.haoblitas.handler.EstudianteHandler;
import com.haoblitas.handler.FacturaHandler;
import com.haoblitas.handler.MatriculaHandler;
import com.haoblitas.handler.PlatoHandler;

//Functional Endpoints
@Configuration
public class RouterConfig {

	@Bean
	public RouterFunction<ServerResponse> rutasPlatos(PlatoHandler handler) {
		return route
				(GET("/v2/platos").or(GET("/v3/platos")), handler::listar)
				.andRoute(GET("/v2/platos/{id}"), handler::listarPorId)
				.andRoute(POST("/v2/platos"), handler::registrar)
				.andRoute(PUT("/v2/platos"), handler::modificar)
				.andRoute(DELETE("/v2/platos/{id}"), handler::eliminar);
	}
	
	@Bean
	public RouterFunction<ServerResponse> rutasClientes(ClienteHandler handler){
		return route
				(GET("/v2/clientes"), handler::listar)
				.andRoute(GET("/v2/clientes/{id}"), handler::listarPorId)
				.andRoute(POST("/v2/clientes"), handler::registrar)
				.andRoute(PUT("/v2/clientes"), handler::modificar)
				.andRoute(DELETE("/v2/clientes/{id}"), handler::eliminar);	
	}
	
	@Bean
	public RouterFunction<ServerResponse> rutasFacturas(FacturaHandler handler){
		return route
				(GET("/v2/facturas"), handler::listar)
				.andRoute(GET("/v2/facturas/{id}"), handler::listarPorId)
				.andRoute(POST("/v2/facturas"), handler::registrar)
				.andRoute(PUT("/v2/facturas"), handler::modificar)
				.andRoute(DELETE("/v2/facturas/{id}"), handler::eliminar);	
	}
	
	@Bean
	public RouterFunction<ServerResponse> rutasEstudiantes(EstudianteHandler handler){
		return route
				(GET("/v2/estudiantes"), handler::listar)
				.andRoute(GET("/v2/estudiantes/{id}"), handler::listarPorId)
				.andRoute(POST("/v2/estudiantes"), handler::registrar)
				.andRoute(PUT("/v2/estudiantes"), handler::modificar)
				.andRoute(DELETE("/v2/estudiantes/{id}"), handler::eliminar);	
	}
	
	@Bean
	public RouterFunction<ServerResponse> rutasCuros(CursoHandler handler){
		return route
				(GET("/v2/cursos"), handler::listar)
				.andRoute(GET("/v2/cursos/{id}"), handler::listarPorId)
				.andRoute(POST("/v2/cursos"), handler::registrar)
				.andRoute(PUT("/v2/cursos"), handler::modificar)
				.andRoute(DELETE("/v2/cursos/{id}"), handler::eliminar);	
	}
	
	@Bean
	public RouterFunction<ServerResponse> rutasMatriculas(MatriculaHandler handler){
		return route
				(GET("/v2/matriculas"), handler::listar)
				.andRoute(GET("/v2/matriculas/{id}"), handler::listarPorId)
				.andRoute(POST("/v2/matriculas"), handler::registrar)
				.andRoute(PUT("/v2/matriculas"), handler::modificar)
				.andRoute(DELETE("/v2/matriculas/{id}"), handler::eliminar);	
	}

}
